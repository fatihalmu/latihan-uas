[![Python 3.6](https://img.shields.io/badge/python-3.6-blue.svg)](https://www.python.org/downloads/release/python-360/) 
[![coverage report](https://gitlab.com/ppw-aafm/crowd-funding/badges/master/coverage.svg)](https://gitlab.com/ppw-aafm/crowd-funding/commits/master)
![pipeline](https://gitlab.com/ppw-aafm/crowd-funding/badges/master/build.svg?job=coverage) 
#### PROJEK TP1 & TP2 PERANCANGAN PEMROGRAMAN WEB 
----

|       **NAMA**       |   **NPM**    | **DEVELOP TP1**  |  **DEVELOP TP2** |
|:---------------------|:------------:|:----------------:|:----------------:|
| Abiyu Muhammad Akmal |  1706984474  | Form Pendaftaran |  Google OAuth - Django   |
| Ahmad Naufal Hilmy   |  1706043613  | Form Donasi      |  About Page                |
| Fatih Al-mutawakkil  |  1706043664  | Program Kami     |  Daftar Donasi User  |
| Mohammad Faisal      |  1706039502  | Halaman Utama    |  Authentication Logic Flow |
----

**HEROKUAPP LINK =** <https://aafm.herokuapp.com/> 