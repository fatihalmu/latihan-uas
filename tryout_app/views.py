from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from .models import model_belanja 
# Create your views here.
def index(request):
	barang_barang = model_belanja.objects.all()
	response = {'item':barang_barang}
	return render(request, 'belanja.html', response)
def add_item(request):
	barang_barang = model_belanja.objects.all()
	if request.method == 'POST':
		nama_barang = request.POST['barang']
		model_barang= model_belanja.objects.create(barang=nama_barang)
		model_barang.save()
		print(model_barang.barang)
	response = {'item':barang_barang}
	return render(request, 'belanja.html', response)
