from django.conf.urls import url
from .views import index, add_item

urlpatterns = [
	# index dan dashboard
	url(r'^$', index, name='index'),
	url(r'^add-item$', add_item, name='add-item'),
]