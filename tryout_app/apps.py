from django.apps import AppConfig


class TryoutAppConfig(AppConfig):
    name = 'tryout_app'
